<?php
use Hidehalo\Nanoid\Client as NanoID;

/**
 * Homz Class.
 */
class Homz {
	private static $initiated = false;

	/**
	 * Initializing.
	 */
	public static function init() {
		if ( ! self::$initiated ) {
			self::init_hooks();
		}
	}
	
	/**
	 * Initializes WordPress hooks
	 */
	private static function init_hooks() {
	}

	/**
	 * View content
	 * @static
	 */
	public static function view( $name, array $args = array() ) {
		$file = HOMZ_PLUGIN_DIR . 'views/'. $name . '.php';
		include( $file );
	}

	/**
	 * Get spinner id with prefix.
	 * @static
	 * @param string $id Spinner id.
	 * @return string
	 */
	public static function get_spinner_id( $id = '' ) {
		return sprintf( 'homz_spinner_%s', $id );
	}

	/**
	 * Get content spinner list.
	 * @static
	 * @return void
	 */
	public static function get_all_spinner() {
		global $wpdb;

		$prefix = self::get_spinner_id();
		$sql    = sprintf(
			'SELECT `%s` as `id`, `%s` as `data` FROM `%s` WHERE `%s` LIKE \'%s\'',
			'option_name',
			'option_value',
			$wpdb->prefix . 'options',
			'option_name',
			'%' . $prefix . '%'
		);

		$items  = array();
		$result = $wpdb->get_results( $sql, ARRAY_A );

		if ( $result ) {
			foreach ( $result as $item ) {
				$id      = str_replace( $prefix, '', $item['id'] );

				$data    = json_decode( $item['data'], true );
				unset( $data['content'] );

				$items[] = array(
					'id'   => $id,
					'data' => $data,
				);
			}

		}

		return $items;
	}

	/**
	 * Add content spinner.
	 * @static
	 * @param array $data  Spinner Data.
	 * @return string
	 */
	public static function add_spinner( $data = array() ) {
		$data = array_merge( array(
			'title'   => 'Untitled spinner',
			'type'    => 'title',
			'time'    => time(),
			'active'  => true,
			'content' => '',
		), $data );

		$nano = new NanoID();
		$id   = $nano->formatedId( '1234567890abcdef', 10 );

		add_option( self::get_spinner_id( $id ), wp_json_encode( $data ), '', 'no' );

		return $id;
	}

	/**
	 * Add content spinner.
	 * @static
	 * @param string $id    Spinner ID.
	 * @param array  $data  Spinner Data.
	 * @return null|bool
	 */
	public static function update_spinner( $id = '', $data = array() ) {
		global $wpdb;

		$id  = self::get_spinner_id( $id );
		$sql = sprintf(
			'SELECT `%s` as `id` FROM `%s` WHERE `%s` LIKE \'%s\'',
			'option_name',
			$wpdb->prefix . 'options',
			'option_name',
			'%' . $id . '%'
		);

		$result = $wpdb->get_results( $sql, ARRAY_A );

		if ( ! $result ) {
			return;
		}

		$data['time'] = time();
		$data         = array_merge( array(
			'title'   => 'Untitled spinner',
			'type'    => 'title',
			'active'  => true,
			'content' => '',
		), $data );

		return update_option( $id, wp_json_encode( $data ) );
	}

	/**
	 * Get content spinner by ID.
	 * @static
	 * @param string $id  Spinner ID.
	 * @return void|null|array
	 */
	public static function get_spinner( $id = '' ) {
		$option = get_option( self::get_spinner_id( $id ) );

		if ( $option ) {
			$option = json_decode( $option, true );
			return $option;
		}
	}

	/**
	 * @static
	 * @param string $id  Spinner ID.
	 * @return bool
	 */
	public static function delete_spinner( $id = '' ) {
		return delete_option( self::get_spinner_id( $id ) );
	}
}
