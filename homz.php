<?php
/*
Plugin Name: Homz
Plugin URI: https://google.com
Description: Scraping Houzz's photos to WordPress.
Version: 1.0
Author: oknoorap
Author URI: https://github.com/oknoorap
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

if ( !function_exists( 'add_action' ) ) {
	echo 'This is plugin area, mate.';
	exit;
}

/**
 * Constants.
 */
define( 'HOMZ_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'HOMZ_PLUGIN_URL', plugins_url( '', __FILE__ ) );

/**
 * Include required files.
 */
require_once( HOMZ_PLUGIN_DIR . 'class.homz.php' );
require_once( HOMZ_PLUGIN_DIR . 'class.homz-rest-api.php' );

/**
 * Init plugin.
 */
add_action( 'init', array( 'Homz', 'init' ) );
add_action( 'rest_api_init', array( 'Homz_REST_API', 'init' ) );

/**
 * Initialize admin.
 */
if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
	require_once( HOMZ_PLUGIN_DIR . 'class.homz-admin.php' );
	add_action( 'init', array( 'Homz_Admin', 'init' ) );
}
