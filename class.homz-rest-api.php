<?php
/**
 * Homz REST API.
 */
require HOMZ_PLUGIN_DIR . '/vendor/autoload.php';

/**
 * Homz REST API Class.
 */
class Homz_REST_API {
	/**
	 * Endpoint namespace.
	 */
	public static $namespace = 'homz';

	/**
	 * API version.
	 */
	public static $version = 'v1';

	/**
	 * Routers
	 */
	public static $routers = array(
		array(
			'route'     => '/url-list',
			'methods'   => WP_REST_Server::CREATABLE,
			'callback'  => array( 'Homz_REST_API', 'get_url_list' ),
		),

		array(
			'route'     => '/item-data',
			'methods'   => WP_REST_Server::CREATABLE,
			'callback'  => array( 'Homz_REST_API', 'get_item_data' ),
		),

		array(
			'route'     => '/post-content',
			'methods'   => WP_REST_Server::CREATABLE,
			'callback'  => array( 'Homz_REST_API', 'post_content' ),
		),

		array(
			'route'     => '/spinner-list',
			'methods'   => WP_REST_Server::READABLE,
			'callback'  => array( 'Homz_REST_API', 'get_spinner_list' ),
		),

		array(
			'route'     => '/get-spinner',
			'methods'   => WP_REST_Server::READABLE,
			'callback'  => array( 'Homz_REST_API', 'get_spinner' ),
		),

		array(
			'route'     => '/add-spinner',
			'methods'   => WP_REST_Server::CREATABLE,
			'callback'  => array( 'Homz_REST_API', 'add_spinner' ),
		),

		array(
			'route'     => '/update-spinner',
			'methods'   => WP_REST_Server::CREATABLE,
			'callback'  => array( 'Homz_REST_API', 'update_spinner' ),
		),

		array(
			'route'     => '/delete-spinner',
			'methods'   => WP_REST_Server::DELETABLE,
			'callback'  => array( 'Homz_REST_API', 'delete_spinner' ),
		),
	);

	/**
	 * Register the REST API routes.
	 */
	public static function init() {
		if ( ! function_exists( 'register_rest_route' ) ) {
			return false;
		}

		$namespace = self::$namespace . '/' . self::$version;

		foreach ( self::$routers as $router ) {
			register_rest_route( $namespace, $router['route'],
				array(
					'methods'  => $router['methods'],
					'callback' => $router['callback'],
				)
			);
		}
	}

	/**
	 * Response shorthand.
	 * @static
	 * @param array|null  $data          Response data.
	 * @param string      $err_message   Custom error message.
	 * @return array
	 */
	public static function response( $data = array(), $err_message = '' ) {
		if ( empty( $err_message ) ) {
			$err_message = __( 'Unknown Error.', 'homz' );
		}

		if ( null === $data ) {
			$data = new WP_Error( 'error', $err_message );
		}

		return rest_ensure_response( $data );
	}

	/**
	 * Valid search filters for style.
	 */
	public static $styles = array(
		'contemporary',
		'eclectic',
		'modern',
		'traditional',
		'asian',
		'beach style',
		'craftsman',
		'farmhouse',
		'industrial',
		'mediterranean',
		'midcentury',
		'rustic',
		'scandinavian',
		'shabby-chic style',
		'southwestern',
		'transitional',
		'tropical',
		'victorian',
	);

	/**
	 * Valid search filters for size.
	 */
	public static $sizes = array(
		'compact',
		'medium',
		'large',
		'expansive',
	);

	/**
	 * Valid search filters for color.
	 */
	public static $colors = array(
		'Red',
		'Orange',
		'Wood Tones',
		'Yellow',
		'Green',
		'Turquoise',
		'Blue',
		'Violet',
		'Pink',
		'Black',
		'Gray',
		'White',
		'Beige',
		'Brown',
	);

	/**
	 * Validate arrays and give output with prefix `--`
	 * @param  string $valid_var   Valid variable in class instance.
	 * @param  array  $input       Array input to validate.
	 * @param  array  $output      Array output to validate.
	 * @static
	 */
	public static function validate_filter( $valid_var, &$input, &$output ) {
		if ( ! empty( $input ) && is_array( $input ) ) {
			foreach ( $input as $value ) {
				if ( in_array( $value, self::$$valid_var, true ) ) {
					$output[] = $value;
				}
			}

			$output = implode( '--', $output );
		}
	}

	/**
	 * Get houzz domain, and replace tld from setting.
	 * @static
	 * @param string $path URL Path.
	 * @return string
	 */
	public static function get_site_url( $path = '' ) {
		$url = parse_url( $path );
		if ( array_key_exists( 'path', $url ) ) {
			$path = $url['path'];
		}

		$path = sprintf(
			'https://www.houzz.%s',
			'com'
		) . $path;

		return $path;
	}

	/**
	 * Search keyword and get URL list.
	 * @param WP_REST_Request $request Request Data.
	 * @static
	 * @return array
	 */
	public static function get_url_list( $request ) {
		if ( null === $request ) {
			return self::response( null, __( 'Invalid request.', 'homz' ) );
		}

		$keyword      = $request->get_param( 'keyword' );
		$styles       = $request->get_param( 'styles' );
		$sizes        = $request->get_param( 'sizes' );
		$color        = $request->get_param( 'color' );
		$style_filter = array();
		$size_filter  = array();
		$color_filter = array();
		
		if ( empty( $keyword ) || ! $keyword ) {
			return self::response( null, __( 'No keyword defined.', 'homz' ) );
		}

		// Build url path `filters`.
		$filters = array();

		self::validate_filter( 'styles', $styles, $style_filter );
		if ( ! empty ( $style_filter ) ) {
			$filters[] = $style_filter;
		}

		self::validate_filter( 'sizes', $sizes, $size_filter );
		if ( ! empty( $size_filter ) ) {
			$filters[] = 'size--' . $size_filter;
		}
		
		self::validate_filter( 'color', $color, $color_filter );
		if ( ! empty( $color_filter ) ) {
			$filters[] = 'color--' . $color_filter;
		}

		if ( ! empty( $filters ) ) {
			$filters = '/' . implode( '/', $filters );
		} else {
			$filters = '';
		}

		/**
		 * Requetst and search keyword.
		 */
		$page_url = sprintf(
			self::get_site_url( '/photos%s/query/%s/nqrwns' ),
			$filters,
			sanitize_title_with_dashes( $keyword )
		);
		$response = array( $page_url );
		$http_res = wp_remote_get( esc_url_raw( $page_url ) );

		if ( is_array( $http_res ) ) {
			$body    = wp_remote_retrieve_body( $http_res );
			$doc     = phpQuery::newDocument( $body );
			$list    = array();

			foreach ( $doc->find('.hz-space-card__photo-title') as $title ) {
				$list[] = pq($title)->attr('href');
			}

			$response = array(
				'keyword' => $keyword,
				'list'    => array_slice( $list, 0, 5 ),
			);
		}

		return self::response( $response );
	}

	/**
	 * Get related keyword and <script /> content or src attribute
	 * from Houzz's item page.
	 *
	 * @static
	 * @param WP_REST_Request $request Request Data.
	 * @return array
	 */
	public static function get_item_data( WP_REST_Request $request ) {
		if ( null === $request ) {
			return self::response( null, __( 'Invalid request.', 'homz' ) );
		}

		$url      = $request->get_param( 'url' );
		$response = array();
		$http_res = wp_remote_get( esc_url_raw( $url ) );

		if ( is_array( $http_res ) ) {
			$body    = wp_remote_retrieve_body( $http_res );
			$doc     = phpQuery::newDocument( $body );
			$list    = array();
			$related = array();

			// Find <script> tag.
			foreach ( $doc->find( 'script' ) as $script ) {
				$src = pq($script)->attr('src');

				if ( null !== $src ) {
					$list[] = array(
						'type'      => 'src',
						'content'   => $src,
					);
				} else {
					$html       = pq($script)->html();
					$attr_type  = pq($script)->attr( 'type' );
					$arr        = array(
						'type'      => 'html',
						'content'   => $html,
					);

					if ( $attr_type ) {
						$arr['attr_type'] = $attr_type;
					}

					$list[] = $arr;
				}
			}

			// Find related keywords.
			foreach ( $doc->find( '#relatedSearches a' ) as $el ) {
				$keyword = pq($el);

				if ( $keyword ) {
					$keyword = $keyword->text();
					$keyword = sanitize_title_with_dashes( $keyword );
					$keyword = str_replace( '-', ' ', $keyword );
					$related[] = $keyword;
				}
			}

			// Find image source.
			$image_url  = '';
			$image_meta = $doc->find( '[property="og:image"]' );

			if ( $image_meta ) {
				$image_url_meta_content = $image_meta->attr('content');

				// Convert small image to larger image.
				// from: https://st.hzcdn.com/simgs/<hash>_3-5040/<seo-url>.jpg
				// to: https://st.hzcdn.com/simgs/<hash>_4-5040/<seo-url>.jpg
				$img_regx  = '/_[0-9]-([0-9]{0,10})/';
				$image_url = preg_replace( $img_regx, '_14-$1', $image_url_meta_content );

				// Check whether image exists or not,
				// If not exists, change to _15.
				stream_context_set_default(
					array(
						'http' => array(
							'method' => 'HEAD',
						),
					)
				);

				// Get headers status.
				// which mean, the first index is integer,
				// and status is HTTP/1.1 200 OK
				$http_status = array();
				$headers     = get_headers( $image_url, 1 );
				if ( is_array( $headers ) ) {
					foreach ( $headers as $key => $value ) {
						if ( ! is_numeric( $key ) ) {
							continue;
						}

						$http_status = $value;
					}
				}

				if ( 'HTTP/1.1 200 OK' !== $http_status ) {
					$image_url = preg_replace( $img_regx, '_15-$1', $image_url_meta_content );
				}
			}

			$response = array(
				'related' => $related,
				'image'   => $image_url,
				'scripts' => $list,
			);
		}

		return self::response( $response );
	}


	/**
	 * Download images and post as content.
	 * @static
	 * @param WP_REST_Request $request  Request Data.
	 * @return array
	 */
	public static function post_content( $request ) {
		if ( null === $request ) {
			return self::response( null, __( 'Invalid request.', 'homz' ) );
		}

		$data    = $request->get_param( 'data' );
		$keyword = $request->get_param( 'keyword' );

		if ( ! isset( $data ) || ! isset( $keyword ) ) {
			return self::response( null, __( 'Request require data.', 'homz' ) );
		}

		// Our plans are below:
		// 1. Create a new post (with title from spinner).
		// 2. Download images from data.
		// 3. Attach image and replace spinner syntax with available data.
		$post_data = array(
			'post_title'    => $keyword,
			'post_content'  => ''
		);

		$post = wp_insert_post( $post_data );
		if ( is_wp_error ( $post ) ) {
			return self::response( null, $post->get_error_message() );
		}

		$post_content = '';

		foreach ( $data as $item ) {
			$image_metadata   = array(
				'post_title'  => 'test title',
				// 'description' => 'description'
			);

			$image_attachment = self::download_image( $post, $item['image'], $image_metadata );

			if ( $image_attachment ) {
				// Update post content with new data.
				$post_content .= wp_get_attachment_image( $image_attachment, 'full' );
			}
		}

		$post_data['post_content'] = $post_content;

		// Update post
		$update_post = wp_update_post( $post_data );
		if ( is_wp_error( $update_post ) ) {
			return self::response( null, $update_post->get_error_message() );
		}

		return self::response( array(
			'code'    => WP_Http::OK,
			'message' => 'success'
		) );
	}

	/**
	 * @static
	 * @param string $image_url  An Image URL.
	 * @param string $post_id    Post ID.
	 * @param array  $metadata   Attachment Metadata.
	 * @return int
	 */
	public static function download_image( $post_id, $image_url, $metadata = array() ) {
		$image = wp_remote_get( $image_url );
		$type  = wp_remote_retrieve_header( $image, 'Content-Type' );
	
		if ( ! $type ) {
			return false;
		}
	
		$image_name     = basename( $image_url );
		$body_response  = wp_remote_retrieve_body( $image );
		$upload_handler = wp_upload_bits( $image_name , '',  $body_response );
	
		/**
		 * Insert as attachment.
		 */
		$attachment = array_merge( array(
			'post_mime_type' => $type
		), $metadata);
	
		$attach_id = wp_insert_attachment( $attachment, $upload_handler['file'], $post_id );

		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		// Generate attachment metadata.
		$attach_data = wp_generate_attachment_metadata( $attach_id, $upload_handler['file'] );
		wp_update_attachment_metadata( $attach_id, $attach_data );

		return $attach_id;
	}

	/**
	 * Get content spinner list.
	 * @static
	 * @return array
	 */
	public static function get_spinner_list() {
		return self::response( Homz::get_all_spinner() );
	}

	/**
	 * Get content spinner by ID
	 */
	public static function get_spinner( $request ) {
		return self::response( Homz::get_spinner( $request->get_param( 'id' ) ) );
	}

	/**
	 * Add new spinner.
	 * @static
	 * @param WP_REST_Request $request  Request data.
	 * @return
	 */
	public static function add_spinner( $request ) {
		if ( null === $request ) {
			return self::response( null, __( 'Invalid request.', 'homz' ) );
		}

		$data = $request->get_param( 'data' );

		if ( ! $data ) {
			return self::response( null, __( 'Invalid data.', 'homz' ) );
		}

		$id = Homz::add_spinner( $data );

		return self::response( array(
			'code'    => WP_Http::OK,
			'message' => 'success',
			'data'    => array(
				'id'  => $id,
			),
		) );
	}

	/**
	 * Update spinner.
	 * @static
	 * @param WP_REST_Request $request  Request data.
	 * @return
	 */
	public static function update_spinner( $request ) {
		if ( null === $request ) {
			return self::response( null, __( 'Invalid request.', 'homz' ) );
		}

		$id   = $request->get_param( 'id' );
		$data = $request->get_param( 'data' );

		if ( ! $id ) {
			return self::response( null, __( 'No ID defined.', 'homz' ) );
		}

		if ( ! $data ) {
			return self::response( null, __( 'Invalid data.', 'homz' ) );
		}

		$update = Homz::update_spinner( $id, $data );

		if ( ! $update ) {
			return self::response( null, __( 'Error updating.', 'homz' ) );
		}

		return self::response( array(
			'code'    => WP_Http::OK,
			'message' => 'success'
		) );
	}


	/**
	 * Delete spinner by ID.
	 * @static
	 * @param WP_REST_Request $request  Request data.
	 * @return
	 */
	public static function delete_spinner( $request ) {
		if ( null === $request ) {
			return self::response( null, __( 'Invalid request.', 'homz' ) );
		}

		$id       = $request->get_param( 'id' );
		$success  = Homz::delete_spinner( $id );
		$response = array();

		if ( $success ) {
			$response = array(
				'code'   => WP_Http::OK,
				'status' => 'success',
			);
		}

		return self::response( $response );
	}
}
