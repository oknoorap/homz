<div class="wrap">
	<h1 class="wp-heading-inline">Homz</h1>
	<p class="description">Scraping Houzz's photos to WordPress</p>
	<hr class="wp-header-end">
	<noscript>You need to enable JavaScript to run this app.</noscript>
	<div id="homz-app"></div>
	<div id="homz-frame"></div>
</div>
