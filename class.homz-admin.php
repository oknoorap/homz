<?php

use Jawira\CaseConverter\Convert as CaseConverter;

/**
 * Homz admin class.
 */
class Homz_Admin {
	private static $initiated = false;

	/**
	 * Initializing.
	 * @static
	 * @return void
	 */
	public static function init() {
		if ( ! self::$initiated ) {
			self::init_hooks();
		}
	}
	
	/**
	 * Initializes WordPress hooks
	 * @static
	 * @return void
	 */
	private static function init_hooks() {
		add_action( 'admin_menu', array( 'Homz_Admin', 'admin_menu' ) );
		add_action( 'admin_enqueue_scripts', array( 'Homz_Admin', 'admin_enqueue_scripts' ) );
	}

	/**
	 * Admin menus.
	 * @static
	 * @return void
	 */
	public static function admin_menu() {
		add_menu_page( 
			__( 'Homz', 'homz' ),
			'Homz',
			'manage_options',
			'homz',
			array( 'Homz_Admin', 'admin_view' ),
			'dashicons-admin-home',
			'59.5'
		); 
	}

	/**
	 * Admin scripts.
	 * @static
	 * @return void
	 */
	public static function admin_enqueue_scripts() {
		$namespace            = Homz_REST_API::$namespace . '/' . Homz_REST_API::$version;
		$rest_url             = get_rest_url( null, $namespace );
		$enqueue_script_name  = '';
		$localize_var         = array(
			'resturl'       => array(),
			'frameInjector' => HOMZ_PLUGIN_URL . '/iframe.php'
		);

		// Convert Homz_REST_API::$routers to camelcase
		// And append it to `localize_var.resturl`
		$restrict_chars = array( '/', '-' );
		foreach ( Homz_REST_API::$routers as $router ) {
			$route  = str_replace( $restrict_chars, '_', $router['route'] );
			$camel  = new CaseConverter( $route );
			$localize_var['resturl'][ $camel->toCamel() ] = $rest_url . $router['route'];
		}

		if ( defined( 'HOMZ_DEVELOP') ) {
			$is_windows           = true === strpos( $_SERVER['HTTP_USER_AGENT'], 'windows' );
			$domain               = $is_windows ? 'react.dev' : 'host.docker.internal';
			$react_server         = 'http://' . $domain . ':3000';
			$react_host           = 'http://localhost:3000';
			$response             = wp_remote_get( $react_server . '/asset-manifest.json' );
			$react_asset_manifest = wp_remote_retrieve_body( $response );

			if ( ! empty( $react_asset_manifest ) ) {
				$react_scripts             = json_decode( $react_asset_manifest, true );
				$is_localize_script_loaded = false;

				foreach ( array_reverse( $react_scripts ) as $script_file => $script_path ) {
					$script_name = explode( '.', $script_file );
					$script_ext  = end( $script_name );

					if ( 'js' === $script_ext ) {
						$react_script_name = 'app-' . sanitize_title_with_dashes( $script_file );
						wp_enqueue_script(
							$react_script_name,
							$react_host . $script_path,
							array(),
							null,
							true
						);

						if ( ! $is_localize_script_loaded ) {
							$enqueue_script_name       = $react_script_name;
							$is_localize_script_loaded = true;
						}
					}
				}
			}
		} else {
			$enqueue_script_name = 'homz';
			wp_enqueue_script(
				$enqueue_script_name,
				HOMZ_PLUGIN_URL . '/assets/js/app.js',
				array(),
				null,
				true
			);
		}

		wp_enqueue_editor();
		wp_localize_script( $enqueue_script_name , 'homz',  $localize_var );
	}

	/**
	 * Admin main view.
	 * @static
	 * @return void
	 */
	public static function admin_view() {
		Homz::view( 'main' );
	}
}
