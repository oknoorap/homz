import React, { Component, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import styled from 'styled-components'
import AutoSizer from 'react-virtualized/dist/commonjs/AutoSizer'
import List from 'react-virtualized/dist/commonjs/List'
import BoxContainer from 'components/box-container'
import DashIcon from 'components/dash-icon'
import ToolBar, { ToolbarButton } from 'components/toolbar'
import { queueTypes } from 'stores/queue'
import Loader from 'components/loader'
import SadIcon from 'components/sad'

const Container = styled(BoxContainer)`
  padding: 0;
  height: calc(100% - 65px);
  top: 40px;
  position: relative;
`

const ListItem = styled.div`
  display: flex;
  align-items: center;
  border-width: 1px;
  border-style: solid;
  border-color: ${props => {
    switch(props.status) {
      case queueTypes.DONE:
        return '#94e994'

      default:
        return '#e5e5e5'
    }
  }};
  border-radius: 2px;
  padding: 15px;
  box-sizing: border-box;
  box-shadow: 2px 2px 10px 1px rgba(0, 0, 0, 0.1);
  flex-grow: 1;
  font-size: 1rem;
  background: ${props => {
    switch (props.status) {
      case queueTypes.WAIT:
        return '#f1f1f1'
      
      case queueTypes.DONE:
        return '#caf0ca'

      case queueTypes.RUN:
        return '#fff'

      default: break
    }
  }};
  color: ${props => {
    switch (props.status) {
      case queueTypes.WAIT:
        return '#a8a8a8'

      case queueTypes.RUN:
      case queueTypes.DONE:
        return 'inherit'

      default: break
    }
  }};

  .loader {
    margin-right: 5px;
  }

  i:before {
    font-size: 1rem;
    display: block;
    width: auto;
    height: auto;
    margin-right: 5px;
    color: ${props => {
      switch (props.status) {
        case queueTypes.DONE:
          return 'green'

        default:
          return 'inherit'
      }
    }}
  }
`

const ScrollingItem = styled(ListItem)`
  display: flex;
  align-items: center;
  color: #c0c0c0;
`

const StartButton = styled(ToolbarButton)`
  i {
    color: #03a65d;
    display: flex;
    align-items: center;
    justify-content: center;
    &:before {
      font-size: 1.3rem;
      height: auto;
      width: auto;
      line-height: 0;
      display: inline-block;
    }
  }
`

const PauseButton = styled(ToolbarButton)`
  margin-left: 5px;

  i {
    color: #f5a320;
  }
`

const StopButton = styled(PauseButton)`
  margin-right: auto;

  i {
    color: #cc0000;
  }
`

const KeywordEditorButton = styled(ToolbarButton)`
  color: ${props => props.disabled ? '#c0c0c0' : null};

  i {
    color: ${props => props.disabled ? '#c0c0c0' : '#0073aa'};
    margin-right: 3px;
  }
`

const NoListWrapper = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  color: #c0c0c0;

  svg {
    width: 10%;
    height: auto;
    fill: #c0c0c0;
    margin-bottom: 10px;
  }
`

@inject('$queue', '$status') @observer
class ScrapingPage extends Component {
  constructor(props) {
    super(props)
    this.list = React.createRef()
    this.listOpts = {
      rowHeight: 60,
      overscanRowCount: 1,
      scrollToIndex: undefined,
      showScrollingPlaceholder: false,
      useDynamicRowHeight: true,
    }
  }

  noRowsRenderer = () => (
    <div>No rows</div>
  )

  rowRenderer = ({index, isScrolling, key, style}) => {
    const newStyle = {
      ...style,
      display: 'flex',
      alignItems: 'center',
      width: 'auto',
      left: 8,
      right: 8
    }

    if (isScrolling) {
      return (
        <div key={key} style={newStyle}>
          <ScrollingItem><DashIcon icon="visibility" /> Loading data...</ScrollingItem>
        </div>
      )
    }

    const { keyword, status } = this.props.$queue.list[index]
    let icon

    switch (status) {
      case queueTypes.WAIT:
        icon = <Loader
          width="15"
          height="15"
          color="#000" />
        break
      
      case queueTypes.DONE:
        icon = <DashIcon icon="yes" />
        break

      case queueTypes.RUN:
        icon = <DashIcon icon="search" />
        break

      default: break
    }
  
    return (
      <div key={key} style={newStyle}>
        <ListItem status={status}>
          {icon}
          {keyword}
        </ListItem>
      </div>
    )
  }

  start = () => {
    this.props.$queue.start()
  }

  manageKeyword = () => {
    if (!this.props.$status.run) {
      this.props.history.push('/main/manage-keyword')
    }
  }

  render() {
    return (
      <Fragment>
        <ToolBar>
          {!this.props.$status.run && (
            <StartButton onClick={this.start}>
              <DashIcon icon="controls-play" /> Start
            </StartButton>
          )}

          {this.props.$status.run && (
            <Fragment>
              <PauseButton onClick={this.pause}>
                <DashIcon icon="controls-pause" /> Pause
              </PauseButton>

              <StopButton onClick={this.stop}>
                <DashIcon icon="no" /> Stop
              </StopButton>
            </Fragment>
          )}
  
          <KeywordEditorButton
            onClick={this.manageKeyword}
            disabled={this.props.$status.run}>
            <DashIcon icon="book" /> Add / Edit Keywords
          </KeywordEditorButton>
        </ToolBar>
  
        <Container time={this.props.$queue.time}>
          <Fragment>
            {this.props.$queue.list.length === 0 && (
              <NoListWrapper>
                <SadIcon />
                Make me happy. Please add some keyword.
              </NoListWrapper>
            )}
            {this.props.$queue.list.length > 0 && (
              <AutoSizer>
                {({ width, height }) => (
                  <List
                    {...this.listOpts}
                    ref={this.list}
                    rowCount={this.props.$queue.list.length}
                    noRowsRenderer={this.noRowsRenderer}
                    rowRenderer={this.rowRenderer}
                    width={width}
                    height={height}
                    style={{
                      padding: '10px 10px 20px'
                    }}
                  />
                )}
              </AutoSizer>
            )}
          </Fragment>
        </Container>
      </Fragment>
    )
  }
}

export default ScrapingPage
