import React from 'react'
import styled from 'styled-components'
import { Route } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import Box from 'components/box'
import routers from './routers'

const StatusBar = styled.div`
  background: #ededed;
  border-top: 1px solid #e5e5e5;
  position: absolute;
  left: 0;
  bottom: 0;
  width: 100%;
  padding: 5px 10px;
  font-size: 0.7rem;
  box-sizing: border-box;
`

const MainPage = ({ $status }) => (
  <Box>
    {
      routers.map(
        (route, index) => (
          <Route
            {...route}
            key={`submenu-${index}`}
          />
        )
      )
    }

    <StatusBar>{$status.text}</StatusBar>
  </Box>
)

export default inject('$queue', '$status')(observer(MainPage))
