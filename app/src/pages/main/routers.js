import ScrapingPage from './scraping'
import ManageKeywordPage from './keyword'

export default [
  {
    path: '/main',
    exact: true,
    component: ScrapingPage
  },

  {
    path: '/main/manage-keyword',
    component: ManageKeywordPage
  }
]
