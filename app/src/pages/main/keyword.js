import React, { Component, Fragment } from 'react'
import ReactDOMServer from 'react-dom/server'
import styled from 'styled-components'
import { inject, observer } from 'mobx-react'
import Dropzone from 'dropzone'
import DashIcon from 'components/dash-icon'
import BoxContainer from 'components/box-container'
import ToolBar, { ToolbarButton } from 'components/toolbar'
import Loader from 'components/loader'

const Wrapper = styled.div`
  position: relative;
  height: 100%;
`

const Placeholder = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  pointer-events: none;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #a7a7a7;

  i {
    margin: 0 2px;
  }
`

const KeywordInput = styled.textarea`
  width: 100%;
  height: 100%;
`

const Container = styled(BoxContainer)`
  height: calc(100% - 65px);
  top: 40px;
  position: relative;
`

const CancelButton = styled(ToolbarButton)`
  a {
    color: initial;
  }

  i {
    color: #cc0000;
    margin-right: -1px;
  }
`

const UploadButton = styled(ToolbarButton)`
  margin-left: auto;
  margin-right: 10px;
`

const SaveButton = styled(ToolbarButton)`
  i {
    color: #03a65d;
    margin-right: -2px;
  }
`

const LoadingWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`

@inject('$queue', '$status') @observer
class KeywordPage extends Component {
  state = {
    hidePlaceholder: false,
    loading: false
  }

  constructor(props) {
    super(props)
    this.input = React.createRef()
    this.uploadButton = React.createRef()
    this.dropzone = null
  }

  componentDidMount() {
    if (this.input.current) {
      this.input.current.focus()
    }

    if (this.props.$queue.input !== '') {
      this.setState({
        hidePlaceholder: true
      })
    }

    if (this.uploadButton.current) {
      const state = {...this.state}
      const dropzone = new Dropzone(this.uploadButton.current, {
        url: 'no-url',
        autoProcessQueue: false,
        acceptedFiles: '.txt',previewTemplate: ReactDOMServer.renderToString(
          <div />
        )  
      })
  
      dropzone.on('addedfile', (...files) => {
        state.loading = true
        state.hidePlaceholder = true
        this.setState({ ...state })

        files.forEach(file => {
          const reader = new FileReader()
          reader.onload = event => {
            const { result } = event.target
            this.props.$queue.input = result

            if (this.props.$queue.list.length > 0) {
              state.loading = false
              state.hidePlaceholder = true
            }

            this.setState({ ...state })
          }
          reader.readAsText(file)
        })
      })
  
      this.dropzone = dropzone
    }
  }

  cancel = () => {
    this.props.history.push('/main')
    this.props.$queue.input = ''
  }

  save = () => {
    this.props.history.push('/main')
    this.props.$queue.kill()
  }

  inputChange = event => {
    this.setState({
      hidePlaceholder: event.target.value !== ''
    })
    this.props.$queue.input = event.target.value
  }

  render() {
    return (
      <Fragment>
        <ToolBar>
          <Fragment>
            <CancelButton onClick={this.cancel}>
              <DashIcon icon="no-alt" /> Clear and Return
            </CancelButton>
  
            <UploadButton ref={this.uploadButton}>
              <DashIcon icon="media-text" /> Upload .TXT File
            </UploadButton>
  
            <SaveButton onClick={this.save}>
              <DashIcon icon="yes" /> Save
            </SaveButton>
          </Fragment>
        </ToolBar>

        <Container>
          <Wrapper>
            {!this.state.hidePlaceholder && (
              <Placeholder>
                <p align="center">Enter or paste your <DashIcon icon="text" /> keyword(s) per line, or<br />
                You can also upload keyword(s) in a <DashIcon icon="media-text" /> .txt file, separated by lines.</p>
              </Placeholder>
            )}

            {this.state.loading && (
              <LoadingWrapper>
                <Loader width="50" height="50" />
              </LoadingWrapper>
            )}

            {!this.state.loading && (
              <KeywordInput ref={this.input} onChange={this.inputChange} value={this.props.$queue.input} />
            )}
          </Wrapper>
        </Container>
      </Fragment>
    )
  }
}

export default KeywordPage
