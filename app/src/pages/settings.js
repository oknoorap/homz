import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import Box from 'components/box'

@inject('$queue') @observer
class SettingsPage extends Component {
  componentDidMount() {
    console.log(this.props.$queue.list)
  }

  render() {
    return (
      <Box full={true}>
        <strong>Settings</strong>
      </Box>
    )
  }
}

export default SettingsPage
