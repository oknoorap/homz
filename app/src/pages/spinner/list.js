import React from 'react'
import styled from 'styled-components'
import { inject, observer } from 'mobx-react'
import { withRouter } from 'react-router-dom'
import dayjs from 'dayjs'
import Loader from 'components/loader'

const Wrapper = styled.div`
  flex-grow: 1;
  overflow-y: auto;
  height: calc(100% - 80px);
  border-top: 1px solid #e5e5e5;
  border-bottom: 1px solid #e5e5e5;
`

const LoadingWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
`

const List = styled.div`
  display: flex;
  flex-direction: column;
`

const ListItem = styled.div`
  display: flex;
  flex-direction: column;
  padding: 8px 5px 6px;
  position: relative;
  box-sizing: border-box;
  border-left: 3px solid ${props => props.selected ? '#0073aa' : 'transparent'};
  background: ${props => props.selected ? '#f2f2f2' : 'none' };
  opacity: ${props => props.disabled ? 0.5 : 1};
  cursor: default;

  &:hover {
    border-color: #0073aa;
    background: #f2f2f2;

    .title {
      color: #000;
    }
  }
`

const Title = styled.div`
  font-size: 0.95rem;
  font-weight: 600;
  color: #606060;
`

const DateType = styled.div`
  display: flex;
  align-items: center;
`

const Date = styled.div`
  color: gray;
  margin-right: 3px;
`

const Type = styled.div`
  background: gray;
  color: #fff;
  border-radius: 3px;
  text-transform: uppercase;
  letter-spacing: 0.04rem;
  font-size: 0.55rem;
  display: inline;
  margin-right: auto;
  padding: 2px 3px;
  line-height: normal;
  font-weight: 800;
`

const Status = styled.div.attrs(props => ({
  title: props.active ? 'Enabled' : 'Disabled'
}))`
  position: absolute;
  right: 8px;
  bottom: 10px;
  border-radius: 50%;
  width: 6px;
  height: 6px;
  background-color: ${props => props.active ? '#07e207' : '#e5e5e5'};
`

const NotFoundWrapper = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: gray;
`

const ListComponent = ({ $spinner, disabled = false, match, history }) => (
  <Wrapper>
    {$spinner.loading.LIST && (
      <LoadingWrapper>
        <Loader width={20} height={20} />
      </LoadingWrapper>
    )}

    {!$spinner.loading.LIST && $spinner.list.length === 0 && (
      <NotFoundWrapper>No content spinners found.</NotFoundWrapper>
    )}

    {!$spinner.loading.LIST && $spinner.list.length > 0 && (
      <List>
        {
          $spinner.list.map(({ id, data }) => (
            <ListItem
              key={`list-${id}`}
              disabled={!data.active}
              selected={
                match.params.id &&
                ($spinner.selected && $spinner.selected.id === id)
              }
              onClick={() => !disabled && history.push(`/spinner/${id}`)}>
              <Title className="title">{data.title}</Title>
              <DateType>
                <Date>{dayjs.unix(data.time).format('DD MMM YYYY')}</Date>
                <Type>{data.type}</Type>
              </DateType>
              <Status active={data.active} />
            </ListItem>
          ))
        }
      </List>
    )}
  </Wrapper>
)

export default withRouter(inject('$spinner')(observer(ListComponent)))
