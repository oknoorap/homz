import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import styled from 'styled-components'
import Box from 'components/box'
import _BoxContainer from 'components/box-container'
import SearchBox from './search-box'
import List from './list'
import NewButton from './new-button'
import Item from './item'
import Loader from 'components/loader'

const BoxContainer = styled(_BoxContainer)`
  position: relative;
  overflow: ${props => props.isLoading ? 'hidden' : null};
`

const Wrapper = styled.div`
  display: flex;
  height: 100%;
`

const LeftPanel = styled.div`
  width: 200px;
  border-right: 1px solid #e5e5e5;
`

const RightPanel = styled.div`
  flex-grow: 1;
`

const NoItemNotice = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: gray;
`

const Loading = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(255, 255, 255, 0.75);
  display: flex;
  align-items: center;
  justify-content: center;
`

@inject('$spinner') @observer
class SpinnerPage extends Component {
  async componentDidMount() {
    await this.props.$spinner.fetchingList()

    if (this.props.$spinner.selected) {
      this.props.history.push(`/spinner/${this.props.$spinner.selected.id}`)
    }
  }

  async componentDidUpdate(prevProps) {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      if (this.props.match.params.id !== 'new') {
        this.props.$spinner.activateItemById(this.props.match.params.id)
      } else {
        this.props.$spinner.deactivateItem()
        this.props.$spinner.setItem(this.props.$spinner.defaultItem)
      }
    }
  }

  createNew = () => {
    this.props.$spinner.deactivateItem()
    this.props.$spinner.setItem(this.props.$spinner.defaultItem)
    this.props.history.push('/spinner/new')
  }

  render() {
    const isNew = this.props.match.params.id === 'new'
    const {
      ITEM: isLoadItem,
      DELETE: isRemoving,
      ADD: isAdding,
      SAVE: isSaving
    } = this.props.$spinner.loading
    const isDisabled = isAdding || isSaving || isRemoving

    return (
      <Box>
        <Wrapper>
          <LeftPanel>
            <SearchBox />
            <List disabled={isLoadItem} />
            <NewButton onClick={this.createNew} />
          </LeftPanel>

          <RightPanel>
            <BoxContainer isLoading={isLoadItem}>
              {!this.props.match.params.id && (
                <NoItemNotice>No spinner was selected.</NoItemNotice>
              )}

              {(isLoadItem || isDisabled) && (
                <Loading>
                  <Loader width={20} height={20} />
                </Loading>
              )}

              {this.props.match.params.id && this.props.$spinner.item && (
                <Item isNew={isNew} />
              )}
            </BoxContainer>
          </RightPanel>
        </Wrapper>
      </Box>
    )
  }
}

export default SpinnerPage
