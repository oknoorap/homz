import React, { Fragment, Component } from 'react'
import styled from 'styled-components'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
import Input from 'components/input'
import Content from 'components/content'
import Option from 'components/option'
import DashIcon from 'components/dash-icon'
import Button from 'components/button'

const Item = styled.div``

const ButtonWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;

  p {
    margin-top: 0;
  }

  button {
    margin-right: 8px !important;

    &:last-of-type {
      margin: 0 !important;
    }
  }
`

@inject('$spinner') @observer
class ItemComponent extends Component {
  static defaultProps = {
    isNew: false,
    data: null
  }

  constructor(props) {
    super(props)
    this.title = React.createRef()
  }

  componentDidMount() {
    if (this.title.current) {
      this.title.current.focus()
    }
  }

  handleChange = newData => {
    const oldData = this.props.$spinner.item

    this.props.$spinner.setItem({
      ...oldData,
      ...newData
    })
  }

  save = async () => {
    if (this.props.isNew) {
      const id = await this.props.$spinner.add()
      this.props.history.push(`/spinner/${id}`)
    } else {
      await this.props.$spinner.save()
    }
  }

  confirmDelete = () => {
    this.props.$spinner.confirmDelete = true
  }

  cancelDelete = () => {
    this.props.$spinner.confirmDelete = false
  }

  delete = async () => {
    const id = await this.props.$spinner.delete(this.props.match.params.id)
    let location = '/spinner'

    if (id) {
      location += `/${id}`
    }

    this.props.history.push(location)
  }

  render() {
    const id = this.props.match.params.id
    const data = this.props.$spinner.item
    const {
      ITEM: isLoadItem,
      DELETE: isRemoving,
      ADD: isAdding,
      SAVE: isSaving
    } = this.props.$spinner.loading
    const isDisabled = isAdding || isSaving || isRemoving

    return (
      <Item>
        <Input
          ref={this.title}
          label="Title"
          placeholder="Spinner Title"
          value={data.title}
          isDisabled={isDisabled}
          onChange={title => this.handleChange({ title })} />

        <Content
          label="Content Spinner / Spintax"
          placeholder=""
          type={data.type}
          value={data.content}
          isDisabled={isDisabled}
          onChange={content => this.handleChange({ content })}
          editorId={id}
          description={(
            <Fragment>Please see syntax list in <Link to="/doc">Documentation</Link> tab.</Fragment>
          )} />

        <Option
          onChange={({ value: type }) => this.handleChange({ type })}
          label="Spinner Type"
          isDisabled={isDisabled}
          items={[
            {
              label: 'Title',
              value: 'title',
              selected: data.type === 'title'
            },

            {
              label: 'Content',
              value: 'content',
              selected: data.type === 'content'
            }
          ]}
          description={(
            <Fragment>
              <DashIcon icon="warning" /> Change between <strong>Type</strong> will remove HTML format.
            </Fragment>
          )} />

        <Option
          onChange={({ value: active }) => this.handleChange({ active })}
          label="Status"
          isDisabled={isDisabled}
          items={[
            {
              label: 'Enable',
              value: true,
              selected: data.active
            },

            {
              label: 'Disable',
              value: false,
              selected: !data.active
            }
          ]}
          description={(
            <Fragment>Disabled spinner will not randomized when you're scraping.</Fragment>
          )} />

        <ButtonWrapper>
          {this.props.$spinner.confirmDelete && (
            <Fragment>
              <p>Are you sure want to delete this spinner?</p>
              <div style={{ width: '100%', flexGrow: true}} />
              <Button
                label="Yes"
                onClick={this.delete}
                classes={[
                  'button-danger',
                  isRemoving ? 'button-disabled' : null
                ]} />

              <Button
                label="No"
                onClick={this.cancelDelete}
                classes={[
                  isRemoving ? 'button-disabled' : null
                ]} />
            </Fragment>
          )}

          {!this.props.$spinner.confirmDelete && (
            <Fragment>
              <Button
                icon="yes"
                onClick={this.save}
                label={
                  isAdding
                    ? 'Saving...'
                    : this.props.isNew
                      ? 'Add New'
                      : 'Save Changes'
                }
                classes={[
                  ! isAdding ? 'button-primary' : null,
                  isDisabled ? 'button-disabled' : null,
                ]} />

                {!this.props.isNew && (
                  <Button
                    icon="trash"
                    label="Delete"
                    onClick={this.confirmDelete}
                    classes={[
                      isDisabled ? 'button-disabled' : null
                    ]} />
                )}
            </Fragment>
          )}
        </ButtonWrapper>
      </Item>
    )
  }
}

export default withRouter(ItemComponent)
