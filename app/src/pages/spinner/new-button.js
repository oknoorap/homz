import React from 'react'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'
import Button from 'components/button'

const Wrapper = styled.div`
  padding: 10px;
  display: flex;

  a, button {
    width: 100%;
    text-align: center;
    text-decoration: none;
  }
`

export default withRouter(({ onClick, match }) => (
  <Wrapper>
    <Button
      onClick={onClick}
      classes={[
        'button',
        match.params.id !== 'new'
          ? 'button-primary'
          : null,
        match.params.id === 'new'
          ? 'button-disabled'
          : null
      ]}
      label="New Content Spinner" />
  </Wrapper>
))
