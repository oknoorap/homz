import React, { Component } from 'react'
import styled from 'styled-components'
import DashIcon from 'components/dash-icon'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
`

const SearchInput = styled.input.attrs({
  type: 'search',
  placeholder: 'Search Spinner'
})`
  border: none !important;
  padding: 5px;
  font-size: 0.9rem;
  box-shadow: none !important;

  &:focus {
    box-shadow: none !important;
  }
`

const IconWrapper = styled.div`
  position: absolute;
  top: 50%;
  right: 5px;
  pointer-events: none;
  transform: translateY(-50%);

  i:before {
    opacity: 0.3;
  }
`

class SearchBox extends Component {
  state = {
    isTyping: false
  }

  constructor(props) {
    super(props)
    this.input = React.createRef()
  }

  onChange = event => {
    this.setState({
      isTyping: event.target.value !== ''
    })
  }

  render() {
    return (
      <Wrapper>
        <SearchInput ref={this.input} onChange={this.onChange} />
        {!this.state.isTyping && (
          <IconWrapper>
            <DashIcon icon="search" />
          </IconWrapper>
        )}
      </Wrapper>
    )
  }
}

export default SearchBox
