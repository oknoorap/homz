import { action, observable } from 'mobx'

export default
class StatusStore {
  constructor(rootStore) {
    this.root = rootStore
  }

  @observable run = false
  @observable text = 'Ready for scraping...'

  @action start() {
    this.run = true
  }

  @action stop() {
    this.run = false
  }
}
