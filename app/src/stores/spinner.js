import { action, observable } from 'mobx'
import HttpStatus from 'http-status-codes'
import request from 'ky'
export default
class SpinnerStore {
  constructor(rootStore) {
    this.root = rootStore
  }

  throwResponse = response => {
    if (response.status && response.status === 'error') {
      throw new Error(res.message)
    }
  }

  defaultItem = {
    title: 'Untitled Spinner',
    content: '',
    type: 'title',
    active: true,
  }

  @observable loading = {
    LIST: false,
    ITEM: false,
    DELETE: false,
    SAVE: false
  }

  @observable list = []

  // Selected list.
  @observable selected = null

  // Selected item data.
  @observable item = null

  // Confirm deleting item.
  @observable confirmDelete = false

  @action findById(id) {
    return this.list.find(({ id: _id }) => id === _id)
  }

  @action activateItem(id) {
    const selected = this.findById(id)

    if (selected) {
      this.selected = selected
    }
  }

  @action deactivateItem() {
    this.selected = {}
  }

  @action async fetchingList() {
    this.loading.LIST = true

    const req = await request.get(this.root.resturl.spinnerList)
    const body = await req.json()
    this.throwResponse(body)

    const list = body.filter(
      ({ data }) => data && Object.keys(data).length > 0
    )

    if (body.length && body.length > 0) {
      this.list = list

      const item = list[0]

      await this.activateItemById(item.id)
    }

    this.loading.LIST = false
  }

  @action async activateItemById(id) {
    this.loading.ITEM = true

    const req = await request.get(this.root.resturl.getSpinner, {
      searchParams: { id }
    })

    const body = await req.json()

    this.activateItem(id)
    this.setItem(body)

    this.loading.ITEM = false
  }

  @action setItem(item) {
    this.item = item
  }

  @action updateItem(id, data) {
    const copylist = [...this.list].map(item => {
      if (item.id === id) {
        item.data = {
          ...this.defaultItem,
          ...data
        }

        delete item.data.content
      }

      return item
    })
    this.list = copylist
  }

  @action async add() {
    this.loading.ADD = true

    const req = await request.post(this.root.resturl.addSpinner, {
      json: {
        data: {
          ...this.defaultItem,
          ...this.item
        }
      }
    })
    const body = await req.json()

    this.throwResponse(body)

    const data = {
      ...this.item,
      time: Math.floor(Date.now() / 1000)
    }

    const { id } = body.data

    this.list.push({
      data,
      id
    })

    this.loading.ADD = false
    return id
  }

  @action async save() {
    this.loading.SAVE = true

    const req = await request.post(this.root.resturl.updateSpinner, {
      json: {
        id: this.selected.id,
        data: {
          ...this.defaultItem,
          ...this.item
        }
      }
    })
    const body = await req.json()
    this.throwResponse(body)
    this.updateItem(this.selected.id, this.item)
    this.loading.SAVE = false
  }

  @action async delete(id) {
    this.loading.DELETE = true

    const req = await request.delete(this.root.resturl.deleteSpinner, {
      json: {
        id
      }
    })
    const body = await req.json()

    let nextId

    if (body && body.code === HttpStatus.OK) {
      const currentIndex = this.list.findIndex(item => item.id === id)
      const prevIndex = currentIndex - 1
      const nextIndex = currentIndex + 1

      if (currentIndex === this.list.length - 1) {
        nextId = this.list[prevIndex] ? this.list[prevIndex].id : null
      } else {
        nextId = this.list[nextIndex] ? this.list[nextIndex].id : null
      }

      if (!nextId) {
        this.item = null
        this.selected = null
      }

      const copylist = [...this.list]
      copylist.splice(currentIndex, 1)

      this.list = copylist
    }

    this.confirmDelete = false
    this.loading.DELETE = false
    return nextId
  }
}
