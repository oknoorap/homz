/* global homz */
import { observable, computed, action } from 'mobx'
import eol from 'eol'
import compact from 'lodash.compact'
import uniq from 'lodash.uniq'
import asyncQueue from 'async/queue'
import MultiThread from 'mthread'
import nanoid from 'nanoid'
import { Base64 } from 'js-base64'
import elementReady from 'element-ready'
import worker from './lib/worker'

export const queueTypes = {
  DONE: 'done',
  RUN: 'run',
  WAIT: 'wait',
}

const statusType = {
  ERROR: 'error',
  SUCCESS: 200
}

const maxthread = 4

export default
class QueueStore {
  constructor(rootStore) {
    this.root = rootStore
  }

  @observable index = 0
  @observable input = ''
  @observable time = 0

  @computed get list () {
    return uniq(
      compact(
        eol.split(this.input)
      )
    ).map((keyword, index) => {
      return {
        keyword,
        index,
        status: queueTypes.WAIT
      }
    })
  }

  lastPathToText = url => {
    url = url.split('/')
    url = url[url.length - 1]
    url = url.replace(/-/g, ' ')
    return url
  }

  /**
   * Parse data from `<script>` tag.
   */
  @action parseData({ queue, data, pageID, url: pageURL }, callback) {
    const { scripts, related, image } = data
    const frameParent = document.getElementById('homz-frame')
    const frame = document.createElement('iframe')
    const html = []
    const done = _data => {
      if (typeof callback === 'function') {
        frame.remove()
        callback(_data)
      }
    }

    const onload = async () => {
      const {
        contentWindow: frameWindow,
        contentDocument: frameDoc
      } = frame

      // For debugging purpos.
      frameWindow.pageID = pageID
      frameWindow.pageURL = pageURL

      const content = frameDoc.getElementById('content')
      
      if (content) {
        const form = frameDoc.querySelector('form')
        content.value = Base64.encode(html.join(''))
        form.submit()
      } else {
        // Wait #scripts element to be loaded.
        await elementReady('#scripts', {
          target: frameDoc
        })

        let itemData = {
          keyword: queue.keyword,
          image,
          related
        }

        const { HZ } = frameWindow
        if (!pageID) {
          done()
          return
        }

        if (!HZ.data) {
          const jsonScript = frameDoc.querySelector('[type="application/json"]')
          if (!jsonScript) {
            done()
            return
          }

          const { data: JSONData } = JSON.parse(jsonScript.textContent)
          const { spaceId } = JSONData.pageContentData
          const { data: spaceStore } = JSONData.stores.data.SpaceStore
          const { data: userStore } = JSONData.stores.data.UserStore
          const { data: lboxStore } = JSONData.stores.data.C2LightboxStore
          const { data: proStore } = JSONData.stores.data.ProfessionalStore
          const { data: labelStore } = JSONData.stores.data.SpaceLabelsStore

          if (!lboxStore.spaces) {
            done()
            return
          }

          // Find `title`, `author id`, `category id`, and `style id`.
          let {
            t: title,
            ow: $authorId,
            pid: $projectId,
            brlnk: bread
          } = lboxStore.spaces[spaceId]
          
          const authorId = parseInt($authorId, 10)
          const projectId = parseInt($projectId, 10)

          // Find link.
          let link = ''
          for(const id in spaceStore) {
            if (Object.prototype.hasOwnProperty.call(spaceStore, id)) {
              const space = spaceStore[id]
              if (space.authorId === authorId && space.projectId === projectId) {
                link = space.link
                title = title
              }
            }
          }

          itemData = {
            ...itemData,
            title,
            link
          }

          // Find author and image location.
          const { displayName: author } = userStore[parseInt(authorId)]
          const { location } = proStore[parseInt(authorId)]

          itemData = {
            ...itemData,
            author,
            location
          }

          // Find style and category.
          let { cat: category, s: style } = bread
          category = this.lastPathToText(category)
          style = this.lastPathToText(style)

          itemData = {
            ...itemData,
            category,
            style
          }

          // Find related keywords.
          let isRelatedFilled = false
          if (labelStore[spaceId]) {
            for (const i in labelStore[spaceId]) {
              if (Object.prototype.hasOwnProperty.call(labelStore[spaceId], i) && !isRelatedFilled) {
                let { labels } = labelStore[spaceId][i]
                itemData.related = labels.map(item => item.text.toLowerCase())
                isRelatedFilled = true
              }
            }
          }

          done(itemData)
          return
        }

        // Find `title`, `link`, `author id`, `category id`, and `breadcrumbs`.
        const {
          t: title,
          l: link,
          ow: authorId,
          cat: catId,
          brlnk: bread
        } = HZ.data.Spaces.get(`${pageID}`)

        itemData = {
          ...itemData,
          title,
          link
        }

        // Find category by category ID.
        const {
          name: category
        } = HZ.data.Categories.getCategoryById(catId)

        itemData = {
          ...itemData,
          category
        }

        // Find style from breadcrumbs.
        const breadcrumbs = bread.crumbs.find(item => item.highlight)
        if (breadcrumbs) {
          itemData.style = breadcrumbs.label
        }

        // Find user data.
        const user = HZ.data.Users.get(authorId)
        if (user) {
          // Find author and image location.
          const {
            d: author,
            loc: location
          } = user
  
          itemData = {
            ...itemData,
            author,
            location
          }
        }

        done(itemData)
      }
    }

    frame.id = nanoid(10)
    frame.src = homz.frameInjector
    frame.onload = onload

    scripts.forEach(item => {
      const attrType = item.attr_type
        ? `type="${item.attr_type}"`
        : ''

      switch (item.type) {
        case 'src':
          html.push(`<script src="${item.content}"></script>`)
          break

        case 'html':
          html.push(`<script ${attrType}>${item.content}</script>`)
          break

        default: break
      }
    })

    frameParent.appendChild(frame)
  }

  /**
   * Start process.
   */
  @action start() {
    this.thread = null

    if (this.list.length === 0) {
      return
    }

    // Make entries by copying from computed variable `list`.
    const entries = [...this.list]

    /**
     * Set entries' status by index.
     * @param {Number} index 
     * @param {queueTypes} type 
     */
    const setStatus = (index, type = queueTypes.WAIT) => {
      if (entries && entries[index]) {
        entries[index].status = type
      }
    }

    this.thread = new MultiThread(worker)

    // Finish off posting content.
    this.thread.on('done-posting', ({ queue, data }) => {
      const status = data.code === statusType.DONE
        ? queueTypes.DONE
        : queueTypes.ERROR

      setStatus(queue.index, status)
    })

    /**
     * Get URL list from search page.
     */
    this.thread.on('url-list', ({ queue, list }) => {
      const postData = []

      const q = asyncQueue((url, done) => {
        const pageURI = url
          .replace(/http(s)?:\/\//g, '')
          .replace(/(.*)\/photo\//g, '')
          .split('-')

        const pageID = parseInt(pageURI[0])
        if (!pageID) {
          done()
        }

        // Send data to `posting-content` job
        this.thread.on(`got-data-${pageID}`, itemData => {
          this.parseData(itemData, data => {
            this.thread.off(`got-data-${pageID}`, () => {
              if (!data) {
                done()
                return
              }

              postData.push(data)
              done()
            })
          })
        })

        // Get data from item page.
        this.thread.send('get-data', {
          queue,
          url,
          pageID,
          reqURL: this.root.resturl.itemData
        })
      }, 1) // Set concurrency limit to 1.

      // Post data from url-list.
      q.drain = () => {
        this.thread.send('post-content', {
          queue,
          data: postData,
          reqURL: this.root.resturl.postContent
        })
      }

      setStatus(queue.index, queueTypes.RUN)
      // q.push(list[0])
      list.forEach(item => {
        q.push(item)
      })
    })

    const q = asyncQueue((queue, done) => {
      let reqAnimFrame = null

      const checkFinishedTask = () => {
        const isFinishied = entries.find(
          item => item.index === queue.index && item.status === queueTypes.DONE
        )

        if (isFinishied) {
          cancelAnimationFrame(reqAnimFrame)
          done()
          return
        }

        this.time = Date.now()
        reqAnimFrame = window.requestAnimationFrame(checkFinishedTask)
      }

      this.thread.send('search', {
        queue,
        reqURL: this.root.resturl.urlList
      })
      reqAnimFrame = window.requestAnimationFrame(checkFinishedTask)
    }, maxthread)

    q.drain = () => {
      this.stop()
    }

    entries.forEach(({ index, keyword, status}) => {
      setStatus(index, queueTypes.WAIT)
      q.push({
        index,
        keyword,
        status
      })
    })
    this.root.status.start()
  }

  @action stop() {
    this.time = Date.now()

    if (this.thread) {
      this.thread.kill()
    }

    if (this.q) {
      delete this.q
    }

    this.root.status.stop()
    this.time = 0
    console.log('finish')
  }

  @action kill() {
    this.stop()
    this.thread = null
  }
}
