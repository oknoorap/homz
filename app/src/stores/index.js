import StatusStore from './status'
import QueueStore from './queue'
import SettingStore from './setting'
import SpinnerStore from './spinner'

class Store {
  constructor() {
    this.status = new StatusStore(this)
    this.queue = new QueueStore(this)
    this.setting = new SettingStore(this)
    this.spinner = new SpinnerStore(this)
    
    const { resturl } = homz
    this.resturl = resturl
  }
}

export default () => new Store()
