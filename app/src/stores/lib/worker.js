/* global importScripts */
import workerCode from './worker-code'
export default workerCode(() => {
  importScripts('https://unpkg.com/mthread/dist/mthread.min.js')
  const thread = new MultiThread(true) // eslint-disable-line

  /**
   * Request to REST API with simple fetch POST.
   *
   * @param {String} url 
   * @param {Object} data 
   */
  const request = (url, data) => {
    const headers = new Headers()
    headers.set('Content-Type', 'application/json')

    const body = JSON.stringify(data)

    return fetch(url, {
      method: 'POST',
      headers,
      body
    })
  }

  /**
   * Search keyword and get URL list.
   */
  thread.on('search', ({ queue, reqURL }) => {
    const { keyword } = queue

    request(reqURL, { keyword })
      .then(res => res.json())
      .then(({ list }) => {
        thread.send('url-list', {
          queue,
          list
        })
      })
  })

  /**
   * Get data from individual page's item.
   */
  thread.on('get-data', ({ queue, url, pageID, reqURL }) => {
    request(reqURL, { url })
      .then(res => res.json())
      .then(data => {
        thread.send(`got-data-${pageID}`, {
          queue,
          url,
          pageID,
          data
        })
      })
  })

  /**
   * Posting content.
   */
  thread.on('post-content', ({ queue, data, reqURL }) => {
    request(reqURL, {
      keyword: queue.keyword,
      data
    })
      .then(res => res.json())
      .then(json => {
        thread.send('done-posting', {
          queue,
          data: json
        })
      })
  })
})
