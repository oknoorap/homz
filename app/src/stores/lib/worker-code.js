const blobToCode = code => {
  const blob = new Blob([code], {
    type: 'application/javascript'
  })

  return URL.createObjectURL(blob)
}

export default fn => {
  if (typeof fn !== 'function') {
    return blobToCode('')
  }

  let code = fn.toString()
  code = code.substring(code.indexOf('{') + 1, code.lastIndexOf('}'))

  return blobToCode(code)
}
