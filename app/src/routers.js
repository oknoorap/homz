import React from 'react'
import { Redirect } from 'react-router-dom'
import MainPage from './pages/main/index'
import SpinnerPage from './pages/spinner/index'
import SettingsPage from './pages/settings'

export default [
  {
    path: '/',
    exact: true,
    default: true,
    render: () => (
      <Redirect to="/main"/>
    )
  },

  {
    link: '/main',
    path: '/main/:submenu?',
    component: MainPage,
    label: 'Queue List',
    icon: 'images-alt'
  },

  {
    link: '/spinner',
    path: '/spinner/:id?',
    component: SpinnerPage,
    label: 'Content Spinner',
    icon: 'controls-repeat'
  },

  {
    path: '/settings',
    component: SettingsPage,
    exact: true,
    label: 'Settings',
    icon: 'admin-generic'
  }
]
