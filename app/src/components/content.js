import React from 'react'
import styled from 'styled-components'
import striptags from 'striptags'
import Label from './label'
import Input, { Wrapper } from './input'

const Content = styled.textarea`
  width: 100%;
  height: 150px;
`

const editorTypes = {
  TITLE: 'title',
  CONTENT: 'content'
}

class ContentComponent extends Input {
  static defaultProps = {
    ...Input.defaultProps,
    onChange(){},
    editorId: 'default',
    type: 'title'
  }

  editorId = 'editor'

  constructor(props) {
    super(props)
    this.input = React.createRef()
  }

  componentDidMount() {
    this.renderEditor()
  }

  componentDidUpdate() {
    this.renderEditor()
  }

  renderTinyMCE = () => {
    const currentEditor = tinymce.get(this.editorId)
    if (currentEditor) {
      currentEditor.setContent(this.props.value)
      return
    }

    wp.editor.initialize(this.editorId, {
      tinymce: {
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : '',
        theme: 'modern',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
        image_advtab: true,
        content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        ],
        selector: this.input.current,
        init_instance_callback: editor => {
          editor.on('KeyUp Change', () => {
            this.onChange(editor.getContent())
          })

          editor.setContent(this.props.value)
        }
      }
    })
  }

  renderTextEditor = () => {
    const editor = tinymce.get(this.editorId)
    if (editor) {
      editor.setContent(striptags(this.props.value))
      wp.editor.remove(this.editorId)
    }
  }

  renderEditor = () => {
    if (this.props.type === editorTypes.CONTENT) {
      this.renderTinyMCE()
    } else {
      this.renderTextEditor()
    }
  }

  onChange = eventValue => {
    this.props.onChange(
      eventValue.target
        ? eventValue.target.value
        : eventValue
    )
  }

  render() {
    const {
      label,
      placeholder,
      description,
      value
    } = this.props

    return (
      <Wrapper>
        <Label>{label}</Label>

        <Content
          ref={this.input}
          id={this.editorId}
          placeholder={placeholder}
          value={value}
          onChange={this.onChange} />

        {description && (
          <p className="description">{description}</p>
        )}
      </Wrapper>
    )
  }
}

export default ContentComponent
