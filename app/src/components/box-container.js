import styled from 'styled-components'

export default styled.div`
  padding: 15px;
  box-sizing: border-box;
  overflow-y: auto;
  height: 100%;
`
