import styled from 'styled-components'


export const ToolbarButton = styled.div`
  font-weight: 800;
  font-size: 0.825rem;
  color: #57595c;
  display: flex;
  align-items: center;
  cursor: pointer;

  a {
    text-decoration: none;
    border: none;
    &:focus {
      box-shadow: none;
    }
  }
`

export default styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  padding: 10px;
  box-sizing: border-box;
  border-bottom: 1px solid #e5e5e5;
  display: flex;
  justify-content: space-between;
`
