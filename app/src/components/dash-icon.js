import React from 'react'
import classnames from 'classnames'

export default props => (
  <i className={classnames([
    'dashicons-before',
    `dashicons-${props.icon}`
  ])} {...props} />
)
