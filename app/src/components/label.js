import styled from 'styled-components'

export default styled.div`
  text-transform: uppercase;
  font-size: 0.65rem;
  letter-spacing: 1px;
  color: gray;
`
