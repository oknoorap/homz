import React, { Component } from 'react'
import styled from 'styled-components'
import Label from './label'

const Wrapper = styled.div`
  margin-bottom: 20px;
`

const Options = styled.div`
  display: flex;
`

const Option = styled.div`
  background: ${props => props.selected ? '#0073aa' : null};
  color: ${props => props.selected ? '#fff' : null};
  border: 1px solid #0073aa;
  border-right: none;
  padding: 5px 10px;
  text-transform: uppercase;
  letter-spacing: 1px;
  cursor: default;
  font-size: 0.65rem;
  font-weight: 800;
  line-height: normal;

  &:last-of-type {
    border-right: 1px solid #0073aa;
  }
`

class OptionComponent extends Component {
  static defaultProps = {
    items: [],
    label: 'Untitled Options',
    description: null,
    onChange(){}
  }

  select = selected => {
    this.props.onChange(this.props.items[selected])
  }

  render() {
    return (
      <Wrapper>
        <Label>{this.props.label}</Label>

        <Options>
          {
            this.props.items.map((item, index) => (
              <Option
                key={`option-${index}`}
                selected={item.selected}
                onClick={() => this.select(index)}>{item.label}</Option>
            ))
          }
        </Options>
        {this.props.description && (
          <p className="description">{this.props.description}</p>
        )}
      </Wrapper>
    )
  }
}

export default OptionComponent
