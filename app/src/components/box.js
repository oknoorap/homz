import styled from 'styled-components'

export default styled.div`
  position: relative;
  height: ${props => props.full ? null : 'calc(100vh - (100vh / 4))'};
  box-sizing: border-box;
  border: 1px solid #e5e5e5;
  box-shadow: 0 1px 1px rgba(0,0,0,.04);
  background: #fff;
  font-family: sans-serif;
`
