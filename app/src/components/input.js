import React, { Component } from 'react'
import styled from 'styled-components'
import Label from './label'

export const Wrapper = styled.div`
  margin-bottom: 20px;
`

const Input = styled.input`
  padding: 5px;
  font-size: 1rem;
  width: 60%;
`

class InputComponent extends Component {
  static defaultProps = {
    label: 'Untitled label',
    type: 'text',
    placeholder: 'Untitled input',
    description: null,
    value: '',
    onChange(){}
  }

  constructor(props) {
    super(props)
    this.input = React.createRef()
  }

  onChange = event => {
    this.props.onChange(event.target.value)
  }

  focus = () => {
    if (this.input.current) {
      this.input.current.focus()
    }
  }

  render() {
    const {
      label,
      type,
      placeholder,
      description,
      value
    } = this.props

    return (
      <Wrapper>
        <Label>{label}</Label>

        <Input
          ref={this.input}
          type={type}
          maxLength={20}
          value={value}
          placeholder={placeholder}
          onChange={this.onChange} />

        {description && (
          <p className="description">{description}</p>
        )}
      </Wrapper>
    )
  }
}

export default InputComponent
