import React from 'react'
import styled, { keyframes } from 'styled-components'

const animation = keyframes`
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  50% {
    -webkit-transform: rotate(180deg);
    transform: rotate(180deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
`

const Eclipse = styled.div`
  position: relative;
  width: ${props => props.width}px;
  height: ${props => props.height}px;
  transform: translate(-${props => props.width}px, - ${props => props.height}px) scale(1) translate(${props => props.width}px, ${props => props.height}px);

  div {
    position: absolute;
    animation: ${animation} 1s linear infinite;
    width: 100%;
    height: 100%;
    border-radius: 50%;
    box-shadow: 0 ${props => (props.width / 4) / 100}rem  0 0 ${props => props.color};
    transform-origin: center;
  }
`

export default ({ width = 100, height = 100, color = '#0073aa' }) => (
  <Eclipse width={width} height={height} color={color} className="loader">
    <div />
  </Eclipse>
)
