import React from 'react'
import styled from 'styled-components'
import DashIcon from './dash-icon'
import classnames from 'classnames'

const Button = styled.button.attrs(props => ({
  className: classnames([
    'button',
    'homz-button',
    ...props.classes
  ])
}))`
  &.button-danger {
    background-color: #be3135;
    background-image: linear-gradient(to bottom, #C5292E, #be3135);
    border-color: #be3135;
    border-bottom-color: #8D1F21;
    box-shadow: inset 0 1px 0 rgba(120,200,230,0.5);
    color: #fff;
    text-decoration: none;
    text-shadow: 0 1px 0 rgba(0,0,0,0.1);

    &:hover,
    &:focus {
      background-color: #B72629;
      background-image: linear-gradient(to bottom, #D22E30, #be3135);
      border-color: #7F1C1F;
      box-shadow: inset 0 1px 0 rgba(120,200,230,0.6);
      color: #fff;
      text-shadow: 0 -1px 0 rgba(0,0,0,0.3);
    }

    &:focus {
      border-color: #500F0E;
      box-shadow: inset 0 1px 0 rgba(120,200,230,0.6), 1px 1px 2px rgba(0,0,0,0.4);
    }

    &:active {
      background: #7F1C1F;
      background-image: linear-gradient(to bottom, #be3135, #B72629);
      border-color: #601312 #AE2426 #AE2426 #AE2426;
      color: rgba(255,255,255,0.95);
      box-shadow: inset 0 1px 0 rgba(0,0,0,0.1);
      text-shadow: 0 1px 0 rgba(0,0,0,0.1);
    }

    &[disabled] {
      color: #E79496 !important;
      background: #BA292B !important;
      border-color: #7F1C1F !important;
      box-shadow: none !important;
      text-shadow: 0 -1px 0 rgba(0,0,0,0.1) !important;
      cursor: default;
    }
  }
`

export default ({
  icon = null,
  label = 'Unlabeled Button',
  classes = [],
  onClick = null,
  disabled = false
}) => (
  <Button classes={classes} onClick={onClick} disabled={disabled}>
    {icon && <DashIcon icon={icon} />} {label}
  </Button>
)
