import React, { Fragment } from 'react'
import { withRouter, Link } from 'react-router-dom'
import classnames from 'classnames'
import { createGlobalStyle }from 'styled-components'
import DashIcon from './dash-icon'
import routers from '../routers'

const GlobalStyle = createGlobalStyle`
  .filter-links {
    a {
      font-size: 1rem;

      &:focus {
        box-shadow: none;
      }

      &.current {
        color: #0073aa;
        border-bottom-color: #0073aa;
      }

      i {
        margin-right: 3px;
      }
    }
  }
`

export default withRouter(({ location, match }) => (
  <Fragment>
    <GlobalStyle />
    <div className="wp-filter">
      <ul className="filter-links">
        {
          routers.map((route, index) => {
            if (route.default) {
              return null
            }

            const pathname = route.link || route.path
            const className = classnames({
              current: location.pathname === pathname || location.pathname.includes(pathname)
            })

            return (
              <li key={`menu-${index}`}>
                <Link to={pathname} className={className}>
                  <DashIcon icon={route.icon} />
                  {route.label}
                </Link>
              </li>
            )
          })
        }
      </ul>
    </div>
  </Fragment>
))
