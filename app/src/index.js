import React, { Component, Fragment } from 'react'
import ReactDOM from 'react-dom'
import { createGlobalStyle } from 'styled-components'
import { HashRouter as Router, Route } from 'react-router-dom'
import { Provider, observer } from 'mobx-react'
import Navigation from 'components/navigation'
import routers from './routers'
import stores from './stores'
import * as serviceWorker from './serviceWorker'

const GlobalStyle = createGlobalStyle`
  #homz-frame {
    display: none;
  }

  .homz-button {
    display: flex !important;
    align-items: center;
    justify-content: center;

    i {
      line-height: normal;
    }
  }
`

@observer
class App extends Component {
  constructor(props) {
    super(props)
    this.store = stores()
  }

  render() {
    const {
      status: $status,
      queue: $queue,
      setting: $setting,
      spinner: $spinner
    } = this.store

    return (
      <Provider $status={$status} $queue={$queue} $setting={$setting} $spinner={$spinner}>
        <Router>
          <Fragment>
            <GlobalStyle />
            <Navigation />

            {
              routers.map(
                (route, index) => <Route {...route} key={`route-${index}`} />
              )
            }
          </Fragment>
        </Router>
      </Provider>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('homz-app'));
serviceWorker.unregister();
