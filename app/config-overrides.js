const {
  override,
  addDecoratorsLegacy,
  disableEsLint,
  disableChunk
} = require("customize-cra")

module.exports = {
  webpack: override(
    addDecoratorsLegacy(),
    disableEsLint(),
    disableChunk()
  )
}
